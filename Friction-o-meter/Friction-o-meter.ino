#include <LiquidCrystal.h>

const byte sensore_1 = A3;
const byte sensore_2 = A4;
const byte sensore_3 = A5;

const byte soglia = 500;

const byte pulsanti = A0;
/*
  Select = 733
  Left = 514
  Up = 144
  Down = 343
  Right = 0
*/

LiquidCrystal lcd (8, 9, 4, 5, 6, 7);

unsigned long int start_1, end_1;   // Istanti assoluti (rispetto all'accensione della scheda)
unsigned long int start_2, end_2;   // di inizio e fine del "volo". La differenza tra gli istanti
unsigned long int start_3, end_3;   // sarà la misura del tempo di volo.

void  setup () {
  lcd.begin (16, 2);                // Inizializzazione dello schermo LCD sul bus I2C
  lcd.clear();
  pinMode (sensore_1, INPUT);
  pinMode (sensore_2, INPUT);
  pinMode (sensore_3, INPUT);
  pinMode (pulsanti, INPUT);

  Serial.begin(115200);
}

void loop () {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Friction-o-meter");
  lcd.setCursor(0, 1);
  lcd.print(" Premere Select ");  

  while (analogRead(pulsanti) < 700 || analogRead(pulsanti) > 800) {  // Attende pressione di select
    ;
  }

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Friction-o-meter");
  lcd.setCursor(0, 1);
  lcd.print("Acquisizione....");  

  start_1 = 0;
  start_2 = 0;
  start_3 = 0;
  end_1 = 0;
  end_2 = 0;
  end_3 = 0;

  while (start_1 == 0 || start_2 == 0 || start_3 == 0 || end_1 == 0 || end_2 == 0 || end_3 == 0) {
    if (start_1 == 0 && analogRead(sensore_1) <= soglia){
      start_1 = millis();
    }
    if (start_1 != 0 && end_1 == 0 && analogRead(sensore_1) >= soglia){
      end_1 = millis();
    }

    if (start_2 == 0 && analogRead(sensore_2) <= soglia){
      start_2 = millis();
    }
    if (start_2 != 0 && end_2 == 0 && analogRead(sensore_2) >= soglia){
      end_2 = millis();
    }

    if (start_3 == 0 && analogRead(sensore_3) <= soglia){
      start_3 = millis();
    }
    if (start_3 != 0 && end_3 == 0 && analogRead(sensore_3) >= soglia){
      end_3 = millis();
    }

    delay(1);
  }
  
  // Stampa i risultati anche su seriale
  Serial.println(((start_2 - start_1) + (end_2 - end_1))/2);
  Serial.println(((start_3 - start_2) + (end_3 - end_2))/2);
  Serial.println(end_1 - start_1);
  Serial.println(end_2 - start_2);
  Serial.println(end_3 - start_3);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Ris:");
  lcd.setCursor(5,0);
  lcd.print(((start_2 - start_1) + (end_2 - end_1))/2);
  lcd.setCursor(10,0);
  lcd.print(((start_3 - start_2) + (end_3 - end_2))/2);
  lcd.setCursor(0, 1);
  lcd.print(end_1 - start_1);
  lcd.setCursor(5, 1);
  lcd.print(end_2 - start_2);
  lcd.setCursor(10, 1);
  lcd.print(end_3 - start_3);

  while (analogRead(pulsanti) < 700 || analogRead(pulsanti) > 800) {  // Ciclo di attesa per mantenere i risultati fino alla pressione di Select
    ;
  }

  while (analogRead(pulsanti) > 700 && analogRead(pulsanti) < 800) {  // Attesa per il rilascio del pulsante
    ;
  }
}

